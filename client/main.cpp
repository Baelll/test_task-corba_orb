#include <QCoreApplication>
#include <iostream>
#include <../msg.hh>
#include <conio.h>

static CORBA::Object_ptr getObjectReference(CORBA::ORB_ptr orb);

static void msg(Echo_ptr e, char* msg)
{
  CORBA::String_var src = (const char*) msg;

  CORBA::String_var dest = (char*)e->echoString(src);

  std::cout << *src;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    try {
        CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);

        //CORBA::Object_var obj = orb->string_to_object(argv[1]);

        CORBA::Object_var obj = getObjectReference(orb);

        Echo_var echoref = Echo::_narrow(obj);

        char clientText;
        while(clientText = _getch())
            msg(echoref, &clientText);

        orb->destroy();
      }
      catch (CORBA::TRANSIENT&) {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the "
             << "server." << std::endl;
      }
      catch (CORBA::SystemException& ex) {
        std::cerr << "Caught a CORBA::" << ex._name() << std::endl;
      }
      catch (CORBA::Exception& ex) {
        std::cerr << "Caught CORBA::Exception: " << ex._name() << std::endl;
      }

    return a.exec();
}

static CORBA::Object_ptr
getObjectReference(CORBA::ORB_ptr orb)
{
  CosNaming::NamingContext_var rootContext;

  try {
    // Obtain a reference to the root context of the Name service:
    CORBA::Object_var obj;
    obj = orb->resolve_initial_references("NameService");

    // Narrow the reference returned.
    rootContext = CosNaming::NamingContext::_narrow(obj);

    if (CORBA::is_nil(rootContext)) {
      std::cerr << "Failed to narrow the root naming context." << std::endl;
      return CORBA::Object::_nil();
    }
  }
  catch (CORBA::NO_RESOURCES&) {
    std::cerr << "Caught NO_RESOURCES exception. You must configure omniORB "
     << "with the location" << std::endl
     << "of the naming service." << std::endl;
    return CORBA::Object::_nil();
  }
  catch (CORBA::ORB::InvalidName& ex) {
    // This should not happen!
    std::cerr << "Service required is invalid [does not exist]." << std::endl;
    return CORBA::Object::_nil();
  }

  // Create a name object, containing the name test/context:
  CosNaming::Name name;
  name.length(2);

  name[0].id   = (const char*) "test";       // string copied
  name[0].kind = (const char*) "my_context"; // string copied
  name[1].id   = (const char*) "Echo";
  name[1].kind = (const char*) "Object";

  try {
    // Resolve the name to an object reference.
    return rootContext->resolve(name);
  }
  catch (CosNaming::NamingContext::NotFound& ex) {
    std::cerr << "Context not found." << std::endl;
  }
  catch (CORBA::TRANSIENT& ex) {
    std::cerr << "Caught system exception TRANSIENT -- unable to contact the "
         << "naming service." << std::endl
     << "Make sure the naming server is running and that omniORB is "
     << "configured correctly." << std::endl;
  }
  catch (CORBA::SystemException& ex) {
    std::cerr << "Caught a CORBA::" << ex._name()
     << " while using the naming service." << std::endl;
  }
  return CORBA::Object::_nil();
}
